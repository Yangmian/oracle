# 实验4：PL/SQL语言打印杨辉三角

学号：202010414124

班级：20软工1班

姓名：杨勉

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```

运行结果：

![1](E:\Oracle 课程\Test\test4\1.png)

### 杨辉三角存储过程建立

```sql
create or replace PROCEDURE    YHTriangle(N IN NUMBER) AS TYPE 
t_number IS VARRAY(100) OF INTEGER NOT NULL; --定义VARRAY类型的数组 i INTEGER; 
j INTEGER; spaces VARCHAR2(30) :=' '; --三个空格，用于打印时分隔数字 
rowArray t_number := t_number(); --声明rowArray为t_number类型的数组并初始化为空 
BEGIN 
DBMS_OUTPUT.PUT_LINE('1'); --先打印第1行 
DBMS_OUTPUT.PUT(RPAD(1,9,' '));--先打印第2行的首个数字1，并用三个空格分隔数字 
DBMS_OUTPUT.PUT(RPAD(1,9,' '));--打印第2行的末尾数字1，并用三个空格分隔数字 
DBMS_OUTPUT.PUT_LINE(''); --打印换行符号 
FOR i IN 1 .. N LOOP --从第3行开始循环到第N行，计算每一行的数字并输出结果 
rowArray.EXTEND; --扩展数组长度，添加新元素，默认为0
END LOOP; 
rowArray(1):=1; --设置第2行首尾两个数为1 
rowArray(2):=1; 
FOR i IN 3 .. N LOOP --从第3行开始循环到第N行，计算每一行的数字并输出结果 
rowArray(i):=1; --设置每行首个数为1 
j:=i-1;
WHILE j>1 LOOP --计算第j行的数字，从右往左计算，直到第2个数字 
rowArray(j):=rowArray(j)+rowArray(j-1); 
j:=j-1; 
END LOOP; 
FOR j IN 1 .. i LOOP --打印第i行的数字 
DBMS_OUTPUT.PUT(RPAD(rowArray(j),9,' '));--打印数字并用三个空格分隔 
END LOOP; DBMS_OUTPUT.PUT_LINE(''); --打印换行符号 
END LOOP; END YHTriangle;
```

运行结果：

![2](E:\Oracle 课程\Test\test4\2.png)

### 创建并运行YHTriangle的SQL语句

```sql
SET SERVEROUTPUT ON SIZE UNLIMITED

EXECUTE hr.YHTriangle(5);
```

运行结果：

![3](E:\Oracle 课程\Test\test4\3.png)

## 实验总结

- 本实验重在学习sql语句的使用，实现杨辉三角sql语句的创建和执行，输出N行杨辉三角。
