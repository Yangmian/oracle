学号：202010414124
班级：软工20级1班
姓名：杨勉

# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

***第1步***
以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

```sql
$ sqlplus system/123@pdborcl
CREATE ROLE con_res_role;
GRANT connect,resource,CREATE VIEW TO con_res_role;
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale default TABLESPACE "USERS";
ALTER USER sale QUOTA 50M ON users;
GRANT con_res_role TO sale;
--收回角色
REVOKE con_res_role FROM sale;
```

> 语句“ALTER USER sale QUOTA 50M ON users;”是指授权sale用户访问users表空间，空间限额是50M。

## 运行效果
[oracle@oracle1 ~]$ sqlplus system/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期四 4月 24 19:38:30 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期四 4月  13 2023 14:21:25 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production

SQL> CREATE ROLE con_res_role;

角色已创建。

SQL> GRANT connect,resource,CREATE VIEW TO con_res_role;

授权成功。

SQL> CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;

用户已创建。

SQL> ALTER USER sale default TABLESPACE "USERS";

用户已更改。

SQL> ALTER USER sale QUOTA 50M ON users;

用户已更改。

SQL> ALTER USER sale QUOTA 50M ON users;

用户已更改。

SQL> GRANT con_res_role TO sale;

授权成功。




***第2步***
新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。

```sql
$ sqlplus sale/123@pdborcl
SQL> show user;
USER is "sale"
SQL>
SELECT * FROM session_privs;
SELECT * FROM session_roles;
SQL>
CREATE TABLE customers (id number,name varchar2(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;
SELECT * FROM customers_view;
NAME
--------------------------------------------------
zhang
wang
```


## 运行结果
SQL> show user;
USER 为 "SALE"
SELECT * FROM session_privs;
SELECT * FROM session_roles;

PRIVILEGE
----------------------------------------
SET CONTAINER
CREATE INDEXTYPE
CREATE OPERATOR
CREATE TYPE
CREATE TRIGGER
CREATE PROCEDURE
CREATE SEQUENCE
CREATE VIEW
CREATE CLUSTER
CREATE TABLE
CREATE SESSION

已选择 11 行。

SQL> 
ROLE
--------------------------------------------------------------------------------
CONNECT
RESOURCE
SODA_APP
CON_RES_ROLE

CREATE TABLE customers (id number,name varchar2(50)) TABLESPACE "USERS" ;
INSERT INTO customers(id,name)VALUES(1,'zhang');
INSERT INTO customers(id,name)VALUES (2,'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;
GRANT SELECT ON customers_view TO hr;

表已创建。

SQL> SELECT * FROM customers_view;

已创建 1 行。

SQL> 
已创建 1 行。

SQL> 
视图已创建。

SQL> 
授权成功。

SQL> 
NAME
--------------------------------------------------
zhang
wang



***第3步***
用户hr连接到pdborcl，查询sale授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SQL> SELECT * FROM sale.customers;
elect * from sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM sale.customers_view;
NAME
--------------------------------------------------
zhang
wang
```

## 运行结果
SQL> SELECT * FROM sale.customers;
SELECT * FROM sale.customers
                   *
第 1 行出现错误:
ORA-00942: 表或视图不存在

SQL> SELECT * FROM sale.customers_view;

NAME
--------------------------------------------------
zhang
wang



***第4步***
> 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
> sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl
SQL> alter user sale  account unlock;
```
## 运行效果
[oracle@oracle1 ~]$ sqlplus hr/666@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期四 5月 4 20:24:32 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝

请输入用户名:  hr
输入口令: 
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝

请输入用户名:  hr
输入口令: 
ERROR:
ORA-01017: 用户名/口令无效; 登录被拒绝

SP2-0157: 在 3 次尝试之后无法连接到 ORACLE, 退出 SQL*Plus

// 返回system中解锁hr用户
SQL> alter user hr  account unlock;

用户已更改。

// 测试hr是否能登录成功
[oracle@oracle1 ~]$ sqlplus hr/123@pdborcl

SQL*Plus: Release 12.2.0.1.0 Production on 星期四 5月 4 20:29:15 2023

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

上次成功登录时间: 星期四 5月  04 2023 20:09:41 +08:00

连接到: 
Oracle Database 12c Enterprise Edition Release 12.2.0.1.0 - 64bit Production



***第5步***

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

以下样例查看表空间的数据库文件，以及每个文件的磁盘占用情况。

```sql
$ sqlplus system/123@pdborcl

SQL>SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SQL>SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

- autoextensible是显示表空间中的数据文件是否自动增加。
- MAX_MB是指数据文件的最大容量。


## 运行结果
如图5.png



***第6步***
## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
SQL>
drop role con_res_role;
drop user sale cascade;
```

## 运行结果
drop role con_res_role;
drop user sale cascade;

角色已删除。

SQL> 
用户已删除。



## 总结
本次实验，我们在pdborcl插接式数据中，创建了一个新的本地角色con_res_role，该角色包含connect和resource角色，同时授权了CREATE VIEW权限，这样任何拥有con_res_role的用户就可以创建表、过程、触发器等对象，并且也可以创建视图。然后，我们创建了一个新用户sale，并将其授予con_res_role角色，为其分配了表空间并设置了限额为50M。最后，我们以sale用户身份连接数据库，测试了创建表、插入数据、创建视图以及查询表和视图数据等操作。






