# 实验5：包，过程，函数的用法

学号：202010414124

班级：20软工1班

姓名：杨勉

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```

## 实验注意事项，完成时间： 2023-05-16日前上交

- 请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的Oracle项目中的test5目录中。
- 上交后，通过这个地址应该可以打开你的源码：https://github.com/你的用户名/oracle/tree/master/test5
- 实验分析及结果文档说明书用Markdown格式编写。

## 实验步骤

#### 1.登录hr用户，创建MyPack包

```sql
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/

```

运行效果：

![1](E:\Oracle 课程\Test\test5\1.png)

#### 2.创建函数Get_SalaryAmount。

```sql
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

```

运行效果：

![2](E:\Oracle 课程\Test\test5\2.png)

3.在MyPack中创建过程GET_EMPLOYEES

```sql
PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```

运行效果：

![3](E:\Oracle 课程\Test\test5\3.png)



## 测试

```sql
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```

运行结果：

![4.1](E:\Oracle 课程\Test\test5\4.1.png)

![4.2](E:\Oracle 课程\Test\test5\4.2.png)



```sql
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/


```

运行效果：

![5](E:\Oracle 课程\Test\test5\5.png)





## 总结

本次实验让我们对PL/SQL语言结构和包、过程、函数有了更深入的了解。我们学习了PL/SQL语言中变量和常量的重要性，以及它们的声明和使用方法。我们使用DECLARE关键字来声明变量和常量，并为它们指定了相应的数据类型。我们还学习了包、过程和函数这三个重要的概念。包是一种组织相关对象的方式，可以将多个过程和函数放在同一个包中，提高代码的组织性和可重用性。过程是一段不返回值的代码块，而函数是一段返回值的代码块。我们可以使用CALL关键字来调用过程和函数，实现特定的功能需求。通过掌握PL/SQL语言结构和变量、常量的声明和使用方法，以及包、过程和函数的应用，我们能够在数据库开发中提高效率和代码质量。PL/SQL提供了丰富的功能和灵活的编程机制，可以更好地处理数据和实现业务逻辑。掌握这些知识将为我们开发稳健可靠的数据库应用程序提供有力的工具。

在实际开发中，我们应该灵活运用这些概念和技巧，以提高数据库开发效率，并确保代码的可维护性和可扩展性。
