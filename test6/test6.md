﻿姓名：杨勉

学号：202010414124

班级：20软工1班

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|





# 基于Oracle数据库的商品销售系统数据库设计方案

### 一.表及表空间设计方案：

- 在Oracle数据库中，创建两个表空间：TS1和TS2，以支持分离和优化存储。相关的SQL脚本在文件 "create_ts.sql" 中。
  - TBS1: 用于存储用户数据。
  - TBS2: 于存储索引和临时数据。

```sql
-- 创建两个表空间
CREATE TABLESPACE TS1 DATAFILE 'ts1.dbf' SIZE 100M;
CREATE TABLESPACE TS2 DATAFILE 'ts2.dbf' SIZE 100M;

```

- 运行结果如图：1.png

![1](E:\Oracle 课程\img\1.png)



- 设计并创建了四个表，相关的SQL脚本在文件 "create_table.sql" 中：
  1. Customers表：存储用户信息，包括用户ID，用户名，密码，电子邮件和地址等。用户ID设为主键，用户名设为唯一键。
  2. Products表：存储商品信息，包括商品ID，商品名称，价格和库存数量等。商品ID设为主键。
  3. Orders表：存储订单信息，包括订单ID，顾客ID，下单时间等。订单ID设为主键，用户ID与Users表的用户ID相关联。
  4. OrderDetails表：存储订单详细信息，包括订单详细ID，订单ID，商品ID，购买数量等。订单详细ID设为主键，订单ID与Orders表的订单ID相关联，商品ID与Products表的商品ID相关联。

```sql
-- 顾客表
CREATE TABLE Customers(
    ID NUMBER(10) PRIMARY KEY,
    CustomerName VARCHAR2(50),
    Password VARCHAR2(50),
    Email VARCHAR2(100)，
    Address VARCHAR2(100)
) TABLESPACE TS1;

-- 商品表
CREATE TABLE Products(
    ID NUMBER(10) PRIMARY KEY,
    ProductName VARCHAR2(50),
    Price NUMBER(10, 2),
    Quantity NUMBER(10)
) TABLESPACE TS1;

-- 订单表
CREATE TABLE Orders(
    ID NUMBER(10) PRIMARY KEY,
    CustomerID NUMBER(10),
    OrderDate DATE,
    FOREIGN KEY (CustomerID) REFERENCES Customers(ID)
) TABLESPACE TS2;

-- 订单详情表
CREATE TABLE OrderDetails(
    ID NUMBER(10) PRIMARY KEY,
    OrderID NUMBER(10),
    ProductID NUMBER(10),
    Quantity NUMBER(10),
    FOREIGN KEY (OrderID) REFERENCES Orders(ID),
    FOREIGN KEY (ProductID) REFERENCES Products(ID)
) TABLESPACE TS2;

```

- 运行结果如图：2.png

![2](E:\Oracle 课程\img\2.png)



### 二.插入模拟数据：

- 利用PL/SQL脚本生成了10万条模拟数据，并通过批量插入的方式将这些数据插入到四个表中。相关的SQL脚本在文件 "data_insert.sql" 中。
- 使用DBMS_RANDOM包中的函数可以用于生成不同类型的随机数，如整数、小数、日期和字符串等。

- 向Customers表中插入100000条数据，每条数据包括ID、Username、Password和Email四个字段。其中，ID字段的值从1到100000递增，Username字段的值为"User"+ID，Password字段的值为"Password"+ID，Email字段的值为"User"+ID+“@email.com”，Address字段的值为"Address"+ID。
- Products、Orders、OrderDetails三表同上操作。

```sql
-- 插入用户数据
DECLARE
    v_id NUMBER(10);
BEGIN
    FOR i IN 1..100000 LOOP
        v_id := i;
        INSERT INTO Customers(ID, CustomerName, Password, Email, Address)
        VALUES (v_id, 'Customer'||v_id, 'Password'||v_id, 'Customer'||v_id||'@email.com', 'Address'||V_id);
    END LOOP;
    COMMIT;
END;
/


-- 插入商品数据
DECLARE
    v_id NUMBER(10);
BEGIN
    FOR i IN 1..100000 LOOP
        v_id := i;
        INSERT INTO Products(ID, ProductName, Price, Quantity)
        VALUES (v_id, 'Product'||v_id, DBMS_RANDOM.VALUE(10, 100), DBMS_RANDOM.VALUE(1, 100));
    END LOOP;
    COMMIT;
END;
/

-- 插入订单数据
DECLARE
    v_id NUMBER(10);
    v_customer_id NUMBER(10);
    v_order_date DATE;
BEGIN
    FOR i IN 1..100000 LOOP
        -- 生成 ID、CustomerID 和 OrderDate 的值
        v_id := i;
        -- 随机生成一个1到1000之间的整数作为CustomerID
        v_customer_id := DBMS_RANDOM.VALUE(1, 1000); 

        -- 生成随机的OrderDate，使用起始日期'2023-05-01'，递增天数生成OrderDate值
        v_order_date := TO_DATE('2023-05-01', 'YYYY-MM-DD') + (i-1); 

        -- 插入数据到 Orders 表
        INSERT INTO Orders (ID, CustomerID, OrderDate)
        VALUES (v_id, v_customer_id, v_order_date);
    END LOOP;
    COMMIT;
END;
/

-- 插入订单数据
DECLARE
    v_id NUMBER(10);
    v_order_id NUMBER(10);
    v_product_id NUMBER(10);
    v_quantity NUMBER(10);
BEGIN
    FOR i IN 1..100000 LOOP
        -- 生成 ID、OrderID、ProductID 和 Quantity 的值
        v_id := i;
        -- 随机生成一个1到100000之间的整数作为OrderID
        v_order_id := DBMS_RANDOM.VALUE(1, 100000); 
        -- 随机生成一个1到1000之间的整数作为ProductID
        v_product_id := DBMS_RANDOM.VALUE(1, 1000); 
         -- 随机生成一个1到10之间的整数作为Quantity
        v_quantity := DBMS_RANDOM.VALUE(1, 10);

        -- 插入数据到 OrderDetails 表
        INSERT INTO OrderDetails (ID, OrderID, ProductID, Quantity)
        VALUES (v_id, v_order_id, v_product_id, v_quantity);
    END LOOP;
    COMMIT;
END;
```

- 运行结果如图：3.1.png、3.2.png、3.3.png

![3.1](E:\Oracle 课程\img\3.1.png)

![3.2](E:\Oracle 课程\img\3.2.png)

![3.3](E:\Oracle 课程\img\3.3.png)



### 三.权限及用户分配方案：

- 创建两个用户相关的SQL脚本在文件 "create_admin.sql" 中。
  - admin1：拥有TBS1表空间的使用权限，可以操作Customers和Products表。
  - admin2：拥有TBS2表空间的使用权限，可以操作Orders和OrderDetails表。

- 在Oracle数据库中创建一个名为admin1的管理员，密码为pw00001，使用表空间TS1作为默认表空间，并在TS1表空间上拥有无限制的配额。
- admin2同理。

```sql
-- 创建两个用户
CREATE USER admin1 IDENTIFIED BY pw00001 DEFAULT TABLESPACE TS1 QUOTA UNLIMITED ON TS1;
CREATE USER admin2 IDENTIFIED BY pw00002 DEFAULT TABLESPACE TS2 QUOTA UNLIMITED ON TS2;
```

- 运行结果如图：4.png

![4](E:\Oracle 课程\img\4.png)



- 对用户的权限进行了精细划分,相关的SQL脚本在文件 "admin_privilege.sql" 中：
  - admin1：赋予了对Customers和Products表的增删改查权限。
  - admin2：赋予了对Orders和OrderDetails表的增删改查权限。
- 将CONNECT和RESOURCE角色授权给admin1用户。CONNECT角色是授予最终用户的典型权利，最基本的权力，能够连接到Oracle数据库中，并在对其他用户的表有访问权限时，做SELECT、UPDATE、INSERT等操作。RESOURCE角色是授予最终用户的典型权利，最基本的权力，能够创建表、序列、过程、函数等对象。
- admin2操作同上

```sql
-- 权限分配
GRANT CONNECT, RESOURCE TO admin1;
GRANT CONNECT, RESOURCE TO admin2;
```

- 运行结果如图：5.png

![5](E:\Oracle 课程\img\5.png)



### 三.程序包及存储过程、函数设计：

- 创建了名为sales_pkg的程序包，其中包含了两个处理业务逻辑的存储过程和函数，相关的SQL脚本在文件 "sales_pkg.sql" 中：
  - create_order：根据用户ID和商品ID创建订单，并将新创建的订单ID返回。
  - calculate_total_prices：根据订单ID计算订单的总价，并返回订单总价。

```sql
-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg AS
  -- 创建订单
  PROCEDURE create_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_order_id OUT NUMBER);

  -- 计算订单总价
  FUNCTION calculate_total_prices(p_order_id IN NUMBER) RETURN NUMBER;
END sales_pkg;
/

-- 实现程序包
CREATE OR REPLACE PACKAGE BODY sales_pkg AS
  -- 创建订单
  PROCEDURE create_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_order_id OUT NUMBER) IS
  BEGIN
    INSERT INTO Orders (ID, CustomerID, OrderDate)
    VALUES (SEQ_ORDERS.NEXTVAL, p_user_id, SYSDATE)
    RETURNING ID INTO p_order_id;

    INSERT INTO OrderDetails (ID, OrderID, ProductID, Quantity)
    VALUES (SEQ_ORDER_DETAILS.NEXTVAL, p_order_id, p_product_id, 1);
    
    COMMIT;
  END create_order;

  -- 计算订单总价
  FUNCTION calculate_total_prices(p_order_id IN NUMBER) RETURN NUMBER IS
    v_total_prices NUMBER(10, 2);
  BEGIN
    SELECT SUM(Price * Quantity)
    INTO v_total_prices
    FROM OrderDetails od
    JOIN Products p ON od.ProductID = p.ID
    WHERE od.OrderID = p_order_id;

    RETURN v_total_prices;
  END calculate_total_prices;
END sales_pkg;
/
```

- 运行结果如图：6.png

![6](E:\Oracle 课程\img\6.png)



### 四.数据库备份方案：

- 设计定期的全量备份策略，将数据库的完整备份保存在独立的存储介质上，以便在需要时进行恢复。结合日志备份，实现增量备份策略，以减少备份时间和存储空间的消耗。定期测试和验证备份的可用性，确保备份数据的完整性和可恢复性。

- 创建了备份脚本，可以按需调整备份的频率和目标路径，以确保数据库的安全性和数据的完整性。相关的SQL脚本在文件 "backup.sql" 中。

```sql
-- 创建目录对象，指定备份存储路径
CREATE OR REPLACE DIRECTORY backup_dir AS '/path/to/backup/folder';
-- 请将 '/path/to/backup/folder' 替换为实际的备份存储路径

-- 创建作业
BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
    job_name        => 'backup_job',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN BACKUP_DATABASE; END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=DAILY',
    enabled         => TRUE,
    comments        => 'Backup the database daily'
  );
END;
/

-- 定义备份存储过程
CREATE OR REPLACE PROCEDURE BACKUP_DATABASE AS
  v_filename VARCHAR2(100);
BEGIN
  -- 生成备份文件名，包含日期和时间
  v_filename := 'backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dmp';
  
  -- 执行备份命令，将数据库导出为数据泵文件
  EXECUTE IMMEDIATE 'EXPDP username/password@database_name DIRECTORY=backup_dir DUMPFILE=' || v_filename;
  
  -- 记录备份日志
  INSERT INTO BackupLogs (BackupFile, BackupDate)
  VALUES (v_filename, SYSDATE);
  
  COMMIT;
END;
/
```

- 运行结果如图：7.png

![7](E:\Oracle 课程\img\7.png)





### 五.项目总结

这个项目的目标是设计一个基于Oracle数据库的商品销售系统的数据库设计方案。在这个项目中，我们完成了以下任务：

- 表及表空间设计方案。我们设计了两个表空间，至少4张表，总的模拟数据量不少于10万条。其中，我们使用了Oracle的数据类型，如VARCHAR2、NUMBER等。
- 设计权限及用户分配方案。我们设计了至少两个用户，并为他们分配了不同的权限。
- 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。我们使用了PL/SQL语言来实现一些复杂的业务逻辑，如订单管理、库存管理等。
- 设计一套数据库的备份方案。我们设计了一套完整的备份方案，包括全量备份和增量备份。

在这个项目中，我们学习了如何设计一个基于Oracle数据库的商品销售系统的数据库设计方案。我们学习了如何设计表及表空间、权限及用户分配方案、程序包、存储过程和函数以及备份方案。
