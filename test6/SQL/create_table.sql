-- 顾客表
CREATE TABLE Customers(
    ID NUMBER(10) PRIMARY KEY,
    CustomerName VARCHAR2(50),
    Password VARCHAR2(50),
    Email VARCHAR2(100)，
    Address VARCHAR2(100)
) TABLESPACE TS1;

-- 商品表
CREATE TABLE Products(
    ID NUMBER(10) PRIMARY KEY,
    ProductName VARCHAR2(50),
    Price NUMBER(10, 2),
    Quantity NUMBER(10)
) TABLESPACE TS1;

-- 订单表
CREATE TABLE Orders(
    ID NUMBER(10) PRIMARY KEY,
    CustomerID NUMBER(10),
    OrderDate DATE,
    FOREIGN KEY (CustomerID) REFERENCES Customers(ID)
) TABLESPACE TS2;

-- 订单详情表
CREATE TABLE OrderDetails(
    ID NUMBER(10) PRIMARY KEY,
    OrderID NUMBER(10),
    ProductID NUMBER(10),
    Quantity NUMBER(10),
    FOREIGN KEY (OrderID) REFERENCES Orders(ID),
    FOREIGN KEY (ProductID) REFERENCES Products(ID)
) TABLESPACE TS2;
