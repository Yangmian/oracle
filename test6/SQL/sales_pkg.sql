-- 创建程序包
CREATE OR REPLACE PACKAGE sales_pkg AS
  -- 创建订单
  PROCEDURE create_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_order_id OUT NUMBER);

  -- 计算订单总价
  FUNCTION calculate_total_prices(p_order_id IN NUMBER) RETURN NUMBER;
END sales_pkg;
/

-- 实现程序包
CREATE OR REPLACE PACKAGE BODY sales_pkg AS
  -- 创建订单
  PROCEDURE create_order(p_user_id IN NUMBER, p_product_id IN NUMBER, p_order_id OUT NUMBER) IS
  BEGIN
    INSERT INTO Orders (ID, CustomerID, OrderDate)
    VALUES (SEQ_ORDERS.NEXTVAL, p_user_id, SYSDATE)
    RETURNING ID INTO p_order_id;

    INSERT INTO OrderDetails (ID, OrderID, ProductID, Quantity)
    VALUES (SEQ_ORDER_DETAILS.NEXTVAL, p_order_id, p_product_id, 1);
    
    COMMIT;
  END create_order;

  -- 计算订单总价
  FUNCTION calculate_total_prices(p_order_id IN NUMBER) RETURN NUMBER IS
    v_total_prices NUMBER(10, 2);
  BEGIN
    SELECT SUM(Price * Quantity)
    INTO v_total_prices
    FROM OrderDetails od
    JOIN Products p ON od.ProductID = p.ID
    WHERE od.OrderID = p_order_id;

    RETURN v_total_prices;
  END calculate_total_prices;
END sales_pkg;
/