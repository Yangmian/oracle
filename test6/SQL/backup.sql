-- 创建目录对象，指定备份存储路径
CREATE OR REPLACE DIRECTORY backup_dir AS '/path/to/backup/folder';
-- 请将 '/path/to/backup/folder' 替换为实际的备份存储路径

-- 创建作业
BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
    job_name        => 'backup_job',
    job_type        => 'PLSQL_BLOCK',
    job_action      => 'BEGIN BACKUP_DATABASE; END;',
    start_date      => SYSTIMESTAMP,
    repeat_interval => 'FREQ=DAILY',
    enabled         => TRUE,
    comments        => 'Backup the database daily'
  );
END;
/

-- 定义备份存储过程
CREATE OR REPLACE PROCEDURE BACKUP_DATABASE AS
  v_filename VARCHAR2(100);
BEGIN
  -- 生成备份文件名，包含日期和时间
  v_filename := 'backup_' || TO_CHAR(SYSDATE, 'YYYYMMDD_HH24MISS') || '.dmp';
  
  -- 执行备份命令，将数据库导出为数据泵文件
  EXECUTE IMMEDIATE 'EXPDP username/password@database_name DIRECTORY=backup_dir DUMPFILE=' || v_filename;
  
  -- 记录备份日志
  INSERT INTO BackupLogs (BackupFile, BackupDate)
  VALUES (v_filename, SYSDATE);
  
  COMMIT;
END;
/