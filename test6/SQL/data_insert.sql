-- 插入用户数据
DECLARE
    v_id NUMBER(10);
BEGIN
    FOR i IN 1..100000 LOOP
        v_id := i;
        INSERT INTO Customers(ID, CustomerName, Password, Email, Address)
        VALUES (v_id, 'Customer'||v_id, 'Password'||v_id, 'Customer'||v_id||'@email.com', 'Address'||V_id);
    END LOOP;
    COMMIT;
END;
/


-- 插入商品数据
DECLARE
    v_id NUMBER(10);
BEGIN
    FOR i IN 1..100000 LOOP
        v_id := i;
        INSERT INTO Products(ID, ProductName, Price, Quantity)
        VALUES (v_id, 'Product'||v_id, DBMS_RANDOM.VALUE(10, 100), DBMS_RANDOM.VALUE(1, 100));
    END LOOP;
    COMMIT;
END;
/

-- 插入订单数据
DECLARE
    v_id NUMBER(10);
    v_customer_id NUMBER(10);
    v_order_date DATE;
BEGIN
    FOR i IN 1..100000 LOOP
        -- 生成 ID、CustomerID 和 OrderDate 的值
        v_id := i;
        -- 随机生成一个1到1000之间的整数作为CustomerID
        v_customer_id := DBMS_RANDOM.VALUE(1, 1000); 

        -- 生成随机的OrderDate，使用起始日期'2023-05-01'，递增天数生成OrderDate值
        v_order_date := TO_DATE('2023-05-01', 'YYYY-MM-DD') + (i-1); 

        -- 插入数据到 Orders 表
        INSERT INTO Orders (ID, CustomerID, OrderDate)
        VALUES (v_id, v_customer_id, v_order_date);
    END LOOP;
    COMMIT;
END;
/

-- 插入订单数据
DECLARE
    v_id NUMBER(10);
    v_order_id NUMBER(10);
    v_product_id NUMBER(10);
    v_quantity NUMBER(10);
BEGIN
    FOR i IN 1..100000 LOOP
        -- 生成 ID、OrderID、ProductID 和 Quantity 的值
        v_id := i;
        -- 随机生成一个1到100000之间的整数作为OrderID
        v_order_id := DBMS_RANDOM.VALUE(1, 100000); 
        -- 随机生成一个1到1000之间的整数作为ProductID
        v_product_id := DBMS_RANDOM.VALUE(1, 1000); 
         -- 随机生成一个1到10之间的整数作为Quantity
        v_quantity := DBMS_RANDOM.VALUE(1, 10);

        -- 插入数据到 OrderDetails 表
        INSERT INTO OrderDetails (ID, OrderID, ProductID, Quantity)
        VALUES (v_id, v_order_id, v_product_id, v_quantity);
    END LOOP;
    COMMIT;
END;
/